const expect = require('chai').expect;
const mylib = require('../src/mylib');

describe("My unit tests", () => {
    before(() => {
        // do something before testing.
    });
    it("Adds 2 + 2 and equals 4", () => {
        expect(mylib.add(2, 2)).to.equal(4);
    });
    it("Fails if divisor is 0", () => {
        // expect something to throw an error.
        expect(() => mylib.divide(9, 0)).to.throw();
    });
    after(() => {
        // do something after testing.
    });
});